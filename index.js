const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.get("/home", (request, response) => {

	response.send("Welcome to the home page!");
});


let users = [];

app.post("/signup", (request, response) => {

	console.log(request.body);

	if(request.body.username !== '' && request.body.password !== ''){

		users.push(request.body);

		response.send(`User ${request.body.username} successfully registered`);
	} else {

		response.send("Please input BOTH username and password");
	}
});

app.get("/users", (request, response) => {

	response.json(users);
});

app.delete("/delete-user", (request, response) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(request.body.username == users[i].username){

			users.pop();

			message = `User ${request.body.username} has beend deleted`;
			break;
		} else {

			message = "User not found";
		}
	}
	response.send(message);
})

app.listen(port, () => console.log(`Server running at port ${port}`));